package com.example.demomybatisplus.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.demomybatisplus.entity.User;

public interface UserMapper extends BaseMapper<User> {
}
