package com.example.demomybatisplus.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@MapperScan("com.example.demomybatisplus.mapper")
public class MybatisConfiguration {
}
