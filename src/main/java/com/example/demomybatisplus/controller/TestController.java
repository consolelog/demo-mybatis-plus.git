package com.example.demomybatisplus.controller;

import com.example.demomybatisplus.entity.User;
import com.example.demomybatisplus.mapper.UserMapper;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@RestController
public class TestController {
    @Resource
    UserMapper userMapper;

    @GetMapping("/test")
    public List<User> test() {
        return userMapper.selectList(null);
    }
}
